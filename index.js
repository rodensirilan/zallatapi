const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const fileUpload = require('express-fileupload');
// Import routes
const authRoute = require('./routes/auth');
const postRoute = require('./routes/posts');
const productRoute = require('./routes/products');
const shopRoute = require('./routes/shops');
const orderRoute = require('./routes/orders');
const roleRoute = require('./routes/roles');
const cartRoute = require('./routes/carts');
const storageRoute = require('./routes/storages');
const addressRoute = require('./routes/address');

const swaggerUi = require('swagger-ui-express');
const session = require('express-session');
// cors when using swagger yaml edit
const cors = require('cors');
const swaggerDocument = require('./swagger.json');
const passport = require('passport');
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const path = require('path')

const {initializePassport} = require('./passport');
initializePassport(passport);
dotenv.config();

var options = {
  explorer: true
};

// Connect DB
mongoose.connect(process.env.DB_CONNECT,
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
  ()=>console.log('Connected to DB!!')
);

//Middlewares
// cors when using swagger yaml edit
app.use(cors());

app.use('/static', express.static(path.join(__dirname, 'public')))

app.use(session({
  secret: process.env.TOKEN_SECRET,
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(fileUpload({
    createParentPath: true
}));

app.use(express.json());

//Route Middlewares
app.use('/api/user', authRoute);
app.use('/api/post', postRoute);
app.use('/api/products', productRoute);
app.use('/api/shops', shopRoute);
app.use('/api/cart', cartRoute);
app.use('/api/orders', orderRoute);
app.use('/api/roles', roleRoute);
app.use('/api/storage', storageRoute);
app.use('/api/address', addressRoute);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

app.listen(3000, () => console.log('Server Up and running'));