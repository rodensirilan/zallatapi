const Joi = require('@hapi/joi');

//Register Validation
const registerValidation = data => {
  const schema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
    name: Joi.string().min(1).required(),
    role: Joi.number().min(1).required(),
    address: Joi.object({
      street: Joi.string().min(2),
      city: Joi.string().min(3),
      country: Joi.string().min(2),
      postal_code: Joi.string().min(3).required(),
    })
  });
  return schema.validate(data);
}

//Login Validation
const loginValidation = data => {
  const schema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required()
  });
  return schema.validate(data);
}
  
//Product Create Validation
const saveProductValidation = data => {
  const schema = Joi.object({
    name: Joi.string().min(2).required(),
    description: Joi.string().min(3),
    shop: Joi.string().min(3).required(),
    price: Joi.number().required(),
    category: Joi.number().required()
  });
  return schema.validate(data);
}

//Shop Create Validation
const saveShopsValidation = data => {
  const schema = Joi.object({
    name: Joi.string().min(2).required(),
    description: Joi.string().min(3),
    user: Joi.string().min(3).required(),
    category: Joi.number().required(),
    address: Joi.object({
      street: Joi.string().min(2).required(),
      city: Joi.string().min(3),
      country: Joi.string().min(2).required(),
      postal_code: Joi.string().min(3).required(),
    })
  });
  return schema.validate(data);
}

const saveOrderValidation = data => {
  const schema = Joi.object({
    user: Joi.string().min(2).required(),
    amount: Joi.number().min(0).required(),
    shipping_address: Joi.string().min(2).required(),
    products: Joi.array().items(Joi.string())
  });
  return schema.validate(data);
}

//Register Role
const roleCreateValidation = data => {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    description: Joi.string().min(3).required(),
    role_number: Joi.number().required()
  });
  return schema.validate(data);
}

//Register Store files
const storeCreateValidation = data => {
  const schema = Joi.object({
    type: Joi.string().required(),
    name: Joi.string().min(1).required(),
    parent: Joi.string().min(1).required()
  });
  return schema.validate(data);
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.saveProductValidation = saveProductValidation;
module.exports.saveShopsValidation = saveShopsValidation;
module.exports.saveOrderValidation = saveOrderValidation;
module.exports.roleCreateValidation = roleCreateValidation;
module.exports.storeCreateValidation = storeCreateValidation;