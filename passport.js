const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const FacebookTokenStrategy = require('passport-facebook-token');
const User = require('./model/User');
const JWT_SECRET = 'asdasdasdasd';
const initializePassport = passport => {

  // JSON WEB TOKENS STRATEGY
  passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: JWT_SECRET,
    passReqToCallback: true
  }, async (req, payload, done) => {
    try {
      // Find the user specified in token
      const user = await User.findById(payload.sub);

      // If user doesn't exists, handle it
      if (!user) {
        return done(null, false);
      }

      // Otherwise, return the user
      req.user = user;
      done(null, user);
    } catch(error) {
      done(error, false);
    }
  }));

  passport.use('googleToken', new GooglePlusTokenStrategy({
      clientID: '312418515138-s14eobf6t203s3i491ubqggtgs5jipk4.apps.googleusercontent.com',
      clientSecret: '1pTniwWnf6yVGfZw6jVlp7GK',
      passReqToCallback: true,
    }, async (req, accessToken, refreshToken, profile, done)=>{
      try {
        if (req.user) {
          // Alreaddy Logged in, we will link account to google's data
          req.user.methods.push('google')
          req.user.google = {
            id: profile.id,
            email: req.body.email
          }
          await req.user.save()
          return done(null, req.user);
        } else {
          // creating the account 
          let existingUser = await User.findOne({ "google.id": profile.id });
          if (existingUser) return done(null, existingUser);
          console.log(profile);
          // Check if we have someone with the same email
          existingUser = await User.findOne({ "local.email": req.body.email })
          if (existingUser) {
            // We want to merge google's data with local auth
            existingUser.methods.push('google')
            existingUser.google = {
              id: profile.id,
              email: req.body.email
            }
            await existingUser.save()
            return done(null, existingUser);
          }
        }
        
        const newUser = new User({
          methods: ['google'],
          google: {
            id: profile.id,
            email: req.body.email
          }
        });
        
        await newUser.save();
        done(null, newUser);

      }catch(e){
        done(e, false, e.message);
      }
    }));
    

  passport.use('facebookToken', new FacebookTokenStrategy({
    clientID: '160996662276305',
    clientSecret: '348a8fb04f2eef9efb02aff343fb5a6a',
    passReqToCallback: true
  }, async (req, accessToken, refreshToken, profile, done) => {
    try {
      console.log('profile', profile);
      console.log('accessToken', accessToken);
      console.log('refreshToken', refreshToken);
      
      if (req.user) {
        // We're already logged in, time for linking account!
        // Add Facebook's data to an existing account
        req.user.methods.push('facebook')
        req.user.facebook = {
          id: profile.id,
          email: profile.emails[0].value
        }
        await req.user.save();
        return done(null, req.user);
      } else {
        // We're in the account creation process
        let existingUser = await User.findOne({ "facebook.id": profile.id });
        if (existingUser) {
          return done(null, existingUser);
        }

        // Check if we have someone with the same email
        existingUser = await User.findOne({ "local.email": profile.emails[0].value })
        if (existingUser) {
          // We want to merge facebook's data with local auth
          existingUser.methods.push('facebook')
          existingUser.facebook = {
            id: profile.id,
            email: profile.emails[0].value
          }
          await existingUser.save()
          return done(null, existingUser);
        }

        const newUser = new User({
          methods: ['facebook'],
          facebook: {
            id: profile.id,
            email: profile.emails[0].value
          }
        });

        await newUser.save();
        done(null, newUser);
      }
    } catch(error) {
      done(error, false, error.message);
    }
  }));
  
  passport.use(new LocalStrategy({
    usernameField: 'email'
  }, async (email, password, done) => {
    try {
      // Find the user given the email
      const user = await User.findOne({ "local.email": email });
      
      // If not, handle it
      if (!user) {
        return done(null, false);
      }
    
      // Check if the password is correct
      const isMatch = await user.isValidPassword(password);
    
      // If not, handle it
      if (!isMatch) {
        return done(null, false);
      }
    
      // Otherwise, return the user
      done(null, user);
    } catch(error) {
      done(error, false);
    }
  }));
}

module.exports.initializePassport = initializePassport;
