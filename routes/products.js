const router = require('express').Router();
const productController = require('../controllers/products');
const passport = require('passport');

router.post('/', passport.authenticate('jwt', { session: false }), productController.post);
router.put('/:id', passport.authenticate('jwt', { session: false }), productController.put);
router.get('/', productController.get);
router.get('/:id', productController.getById);
router.post('/search', productController.search);
module.exports = router;
