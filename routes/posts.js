const router = require('express').Router();

router.get('/', (req, res) => {
  res.json({
    posts: {
      title: 'test post',
      description: 'random test page which cant be accessed if not logged in'
    }
  });
});

module.exports = router;