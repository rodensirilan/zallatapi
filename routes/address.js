const router = require('express').Router();
const addressController = require('../controllers/address');
const passport = require('passport');

router.get('/', addressController.get);
router.post('/', addressController.post);
router.put('/', passport.authenticate('jwt', { session: false }), addressController.put);
router.delete('/', addressController.delete);
router.post('/bindToUser', addressController.bindToUser);
router.post('/bindToShop', addressController.bindToShop);
router.post('/addAddressBookToUser', addressController.addAddressBookToUser);
router.post('/addAddressBookToShop', addressController.addAddressBookToShop);
module.exports = router;
