const router = require('express').Router();
const shopController = require('../controllers/shops');
const passport = require('passport');

router.post('/nearme', passport.authenticate('jwt', { session: false }), shopController.near);
router.post('/', shopController.post);
router.put('/:id', passport.authenticate('jwt', { session: false }), shopController.put);
router.get('/', shopController.get);
router.get('/:id', shopController.getById);
router.delete('/:id', shopController.remove);
router.post('/search', shopController.search);
module.exports = router;
