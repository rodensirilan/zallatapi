const router = require('express').Router();
const cartController = require('../controllers/carts');
router.get('/', cartController.get);
router.post('/', cartController.post);
router.put('/', cartController.put);
router.post('/checkout', cartController.charge);
router.delete('/', cartController.delete);
module.exports = router;

