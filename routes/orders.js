const router = require('express').Router();
const verify = require('./verifyToken');
const orderController = require('../controllers/orders');

router.get('/', orderController.get);
router.post('/', orderController.post);
router.delete('/:id', orderController.remove);
router.post('/filter', orderController.filter);
router.post('/date_range', orderController.date_range);
router.put('/:id', orderController.put);
router.post('/nearBuyers', orderController.nearOrder);
router.post('/setBuyer', orderController.setBuyer);
router.get('/:id', orderController.getById);
module.exports = router;
