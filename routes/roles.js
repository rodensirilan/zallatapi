const router = require('express').Router();
const roleController = require('../controllers/roles');
const passport = require('passport');

router.post('/', roleController.create);
router.get('/', roleController.get);
router.put('/:id', passport.authenticate('jwt', { session: false }), roleController.put);
module.exports = router;
