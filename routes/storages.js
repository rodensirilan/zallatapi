const router = require('express').Router();
const storagesController = require('../controllers/storages');
const passport = require('passport');

router.post('/', storagesController.create);
router.get('/', storagesController.get);
router.delete('/:id', storagesController.remove);
router.post('/modify', passport.authenticate('jwt', { session: false }), storagesController.edit);
router.get('/parent/:id', storagesController.getByParentId);
module.exports = router;
