# Installation

Zallat API requires [Node.js](https://nodejs.org/) and mongo db to run.

Install the dependencies and devDependencies and start the server.

```sh
$ npm install
$ npm start
```

View swagger url on : domain.com:3000/api-docs

example: http://localhost:3000/api-docs
