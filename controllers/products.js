const Product = require('../model/Product');
const Shop = require('../model/Shop');
const { saveProductValidation } = require('../validation');
const _ = require('lodash');
const { product_search_limit } = require('../config');
module.exports = {
  post: async (req, res) => {
    // validate data before making product
    const {error} = saveProductValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
  
    // Create a new product
    const product = new Product({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      shop: req.body.shop,
      user: req.user.id,
      category: req.body.category
    });
    try {
      const savedProduct = await product.save();
      res.status(201).send({product: product._id});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  
  put: async (req, res) =>{
    // validate data before making product
    const {error} = saveProductValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    const product = await Product.findOne({_id: req.params.id});
    if(!product) return res.status(400).send('Product not found');
    
    product.modified_by = req.user._id;
    product.name = req.body.name;
    product.description = req.body.description;
    product.category = req.body.category;
    product.shop = req.body.shop;
    product.user = req.body.user;
    product.price = req.body.price;
    
    try {
      const savedProduct = await product.save();
      res.status(201).send(savedProduct);
    } catch (err) {
      res.status(400).send(err);
    }    
  },
  
  get: async (req, res) => {
    try {
      const productLists = await Product.find();
      if(!productLists) return res.status(400).send('Product List is Empty');
      
      res.status(200).json(productLists);
      
    } catch (err) {
      res.status(400).send(err);
    }  
  },
  
  getById: async (req, res) => {
    try {
      const productLists = await Product.findOne({_id: req.params.id});
      if(!productLists) return res.status(400).send('Product List is Empty');
      
      res.status(200).json(productLists);
    } catch (err) {
      res.status(400).send(err);
    }    
  },
  
  search: async (req, res) => {
    // here should trap if type and data req is right
    // ex: type=1 should require text requestBody
    // ex#2: type=2 should requre lat and lng requestBodies
    
    try {
      /* let result;
      switch(req.body.type) {
        case 1:
          result = await Product.find(
            {$text: {$search: req.body.text}},
            {score: {$meta: "textScore"}})
            .sort({score:{$meta:"textScore"}})
        break;
        case 2:
          result = await Promise.all(_.map( await Shop.aggregate().near({
            near: [parseFloat(req.body.lng), parseFloat(req.body.lat)],
            maxDistance: 1,
            spherical: true,
            distanceField: "dist"
          }), async shop => await Product.findOne({shop: shop._id})));
        break;
        case 3:
          result = await Product.find({category:req.body.text})
        break;
      }
      */
      let result = await Product.find(
        {$text: {$search: req.body.text}},
        {score: {$meta: "textScore"}})
        .sort({score:{$meta:"textScore"}})
        .limit(product_search_limit)

      if(!result) return res.status(400).send('Product List is Empty');
     
      res.status(200).json(result);
    } catch (err) {
      res.status(400).send(err);
    }    
  }
}