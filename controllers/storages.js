const Storage = require('../model/Storage');
const { storeCreateValidation } = require('../validation');
const sizeOf = require('image-size');
const { uploadFolder } = require('../config');

module.exports = {
  create: async (req, res) => {
    let re = /(?:\.([^.]+))?$/;
    let upfile = req.files.upfile;
    let ext = re.exec(upfile.name)[1];
    // check file types
    if(ext !== 'csv' && upfile.mimetype.split('/')[0] !== 'image')
      res.status(400).send("invalid file type");
    
    // validate data before making storage
    const {error} = storeCreateValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    try {
      let filePath = uploadFolder + Math.round((new Date()).getTime() / 1000) + '_' + upfile.name;
      await upfile.mv(filePath);

      // Check file size if image
       let dimensions = sizeOf(filePath);
       
       if( (req.body.type == 'product_thumbnail' || req.body.type == 'shop_tumbnail') && dimensions.width !== 320 && dimensions.height !== 320)
         return res.status(400).send("invalid thumbnail (320x320) size");
        
       if(req.body.type == 'product_gallery' && dimensions.width !== 1024 && dimensions.height !== 1024)
         return res.status(400).send("invalid gallery image (1024x1024) size");
        
      // Create a new storage
      const storage = new Storage({
        type: req.body.type,
        name: req.body.name,
        parent: req.body.parent,
        path: filePath
      });

      const saveStorage = await storage.save();
      res.status(201).send({storage: storage._id});

      //res.status(201).send('ok');
    } catch (err) {
      res.status(400).send(err);
    }
  },
  edit: async (req, res) => {
    
    let re = /(?:\.([^.]+))?$/;
    let upfile = req.files.upfile;
    let ext = re.exec(upfile.name)[1];
    // check file types
    if(ext !== 'csv' && upfile.mimetype.split('/')[0] !== 'image')
      res.status(400).send("invalid file type");
    
    // validate data before making storage
    //const {error} = storeCreateValidation(req.body);
    //if(error) return res.status(400).send(error.details[0].message);
    
    try {
      let filePath = uploadFolder + Math.round((new Date()).getTime() / 1000) + '_' + upfile.name;
      
      await upfile.mv(filePath);
      
      // Check file size if image
       let dimensions = sizeOf(filePath);
      
       if( (req.body.type == 'product_thumbnail' || req.body.type == 'shop_tumbnail') && dimensions.width !== 320 && dimensions.height !== 320)
         return res.status(400).send("invalid thumbnail (320x320) size");
        
       if(req.body.type == 'product_gallery' && dimensions.width !== 1024 && dimensions.height !== 1020)
         return res.status(400).send("invalid gallery image (1024x1024) size");
       // search the storage
    

       const storage = await Storage.findById(req.body.id)
       
       if (!storage) return res.status(400).send("product not found");
      // populate the storage
        storage.modified_by = req.user._id;
        storage.type = req.body.type;
        storage.name = req.body.name;
        storage.parent = req.body.parent;
        storage.path = filePath;

      const saveStorage = await storage.save();
      
      res.status(201).send({saveStorage});

      //res.status(201).send('ok');
    } catch (err) {
      res.status(400).send(err);
    }
  },
  get: async (req, res) => {
    const storages = await Storage.find({status:1});
    
    if(!storages) return res.status(400).send('storages doesnt exist');
    res.status(200).send(storages);
  },
  
  getByParentId: async (req, res) => {
    if(!req.params.id) return res.status(400).send('storages doesnt exist');
    const storages = await Storage.find({status:1, parent: req.params.id});
    
    if(!storages) return res.status(400).send('storages doesnt exist');
    res.status(200).send(storages);
  },
  
  remove: async (req, res) => {
    if(!req.params.id) return res.status(400).send('storages doesnt exist');
    
    const storage = await Storage.findOne({_id: req.params.id});
    storage.status = 0;
    
    try {
      const saveStorage = await storage.save();
      res.status(201).send(saveStorage);
    } catch (err) {
      res.status(400).send(err);
    }  
  }
}
