const Cart = require('../model/Cart');
const Product = require('../model/Product');
const User = require('../model/User');
const Order = require('../model/Order');
const _ = require('lodash');
const { stripe_secret } = require('../config');
const stripeSecretKey = stripe_secret;
const stripe = require('stripe')(stripeSecretKey)

module.exports = {
  charge: async (req, res) => {
    try {
      const { cart, token } = req.body;
      const cartItem = await Cart.findOne({ status:1, _id: cart }).populate('products.id').lean();
      let shopId = ""
      
      let amount = 0;
      let products = _.map(cartItem.products, item=>{
        let product = item.id
        shopId = product.shop
        
        amount += product.price * item.quantity;
        return {
          id: product._id,
          quantity: item.quantity
        }
      })

      let stripeCharge = await stripe.charges.create({
        amount : amount * 100,
        source: token,
        currency: 'usd'
      })
      const order = new Order({
        user: cartItem.user,
        shipping_address: '1410 olympic way se CA',
        amount,
        shop: shopId,
        products
      });
      const savedOrder = await order.save();
      res.status(200).json({
       // outcome: stripeCharge.outcome,
        status: stripeCharge.status,
        receipt: stripeCharge.receipt_url,
        payment_method: stripeCharge.payment_method_details,
       // id: stripeCharge.id,
       // balance_transaction: stripeCharge.balance_transaction,
        amount        
      });
    } catch (err) {
      res.status(400).json(err);
    }
  },
  
  get: async (req, res) => {

    //find cart
    const cart = await Cart.find({ status:1 }).populate('products');
    if(!cart) return res.status(400).send('cart not found');
    
    try {
     
      res.status(200).json({cart});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  post: async (req, res) => {

    // Create a new cart
     const cart = new Cart({
      user: req.body.user,
      products: req.body.products
    });
    
    try {
      const savedCart = await cart.save();
      res.status(200).json({savedCart});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  put: async (req, res) => {

    //find cart
    const cart = await Cart.findOne({ _id: req.body.id });
    if(!cart) return res.status(400).send('cart not found');
    
    cart.user = req.body.user;
    cart.products = req.body.products;
    
    try {
      const savedCart = await cart.save();
      res.status(200).json({savedCart});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  delete: async (req, res) => {
console.log(req.body.id);
    //find cart
    const cart = await Cart.findOne({ _id: req.body.id });
    if(!cart) return res.status(400).send('cart not found');
    
    cart.status = 0;
    
    try {
      const savedCart = await cart.save();
      res.status(200).json("ok");
    } catch (err) {
      res.status(400).send(err);
    }
  }
}