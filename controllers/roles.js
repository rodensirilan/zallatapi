const Role = require('../model/Role');
const { roleCreateValidation } = require('../validation');

module.exports = {
  create: async (req, res) => {
    // validate data before making roles
    const {error} = roleCreateValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Checking if the role is already in the database
    const roleExist = await Role.findOne({email: req.body.role_number});
    if(roleExist) return res.status(400).send('Role already exist');

    // Create a new role
    const role = new Role({
      name: req.body.name,
      description: req.body.description,
      role_number: req.body.role_number
    });

    try {
      const savedRole = await role.save();
      res.status(201).send({role: role._id});
    } catch (err) {
      res.status(400).send(err);
    }
  },

  put: async (req, res) => {
    // validate data before making roles
    const {error} = roleCreateValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    try {   
      const role = await Role.findById(req.params.id);
      if(!role) return res.status(400).send('Role not found');

      // Checking if the role is already in the database
      const roleExist = await Role.findOne({role_number: req.body.role_number, _id: { $ne: req.params.id }});
      if(roleExist) return res.status(400).send('Role already exist');
      
      role.modified_by = req.user._id;
      role.name = req.body.name;
      role.description = req.body.description;
      role.role_number = req.body.role_number;

      const savedRole = await role.save();
      res.status(201).send(savedRole);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  
  get: async (req, res) => {
    const role = await Role.find();
    if(!role) return res.status(400).send('Role doesnt exist');
    res.status(200).send(role);
  },
}
