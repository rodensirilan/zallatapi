const Shop = require('../model/Shop');
const User = require('../model/User');
const Product = require('../model/Product');
const { saveShopsValidation } = require('../validation');
const _ = require('lodash');
const axios = require('axios')
const { shop_limit, shop_search_limit } = require('../config');

var theEarth = (function() {

  var earthRadius = 6371; // km, miles is 3959

  var getDistanceFromRads = function(rads) {
    return parseFloat(rads * earthRadius);
  };

  var getRadsFromDistance = function(distance) {
    return parseFloat(distance / earthRadius);
  };

  return {
    getDistanceFromRads: getDistanceFromRads,
    getRadsFromDistance: getRadsFromDistance
  };
})();

module.exports = {
  near: async (req,res) => {
    
    let postal_code = req.user.address.postal_code;
     
    let response = await axios.get(process.env.GMAP_URL+postal_code)
    let googleResponse = JSON.parse(JSON.stringify(response.data))

    if (googleResponse.status == "ZERO_RESULTS") return res.status(400).json({error:{message: "invalid postal_code"}})
    let lat = googleResponse.results[0].geometry.location.lat;
    let lng = googleResponse.results[0].geometry.location.lng;

    let result = await Shop.aggregate([{
     $geoNear: {
        near: { type: "Point", coordinates: [ lng,
              lat ] },
        distanceField: "dist.calculated",
        maxDistance: req.body.distance * 1000,
        spherical: true
     }
   }]).limit(shop_limit);
    
   let newr = await Promise.all(_.map(result, async (place) => {
    let distReponse = await axios.get(`${process.env.GMAP_DISTANCE}destinations=${place.geometry.coordinates[1]},${place.geometry.coordinates[0]}&origins=${lat},${lng}`)
    if(distReponse.data.rows[0].elements[0].status !== "NOT_FOUND") {
      return {
        _id: place._id,
        name: place.name,
        street: place.address.street,
        city: place.address.city,
        country: place.address.country,
        postal_code: place.address.postal_code,
        distance : distReponse.data.rows[0].elements[0].distance.value,
        duration: distReponse.data.rows[0].elements[0].duration.value
      }
    }
   }));

   return res.status(200).json( _.orderBy(newr, ['distance'], ['asc']));
  },
  
  post: async (req, res) => {
    // validate data before making shop
    const {error} = saveShopsValidation(req.body);
    if(error) return res.status(400).send("There was an error");
     const { name, description, user, address, category } = req.body;
    // check zip for lat lng
    let response = await axios.get(`${process.env.GMAP_URL}${address.street},+${address.country}`)
    let googleResponse = JSON.parse(JSON.stringify(response.data))
    
    if (googleResponse.status == "ZERO_RESULTS") return res.status(400).json({error:{message: "invalid postal_code"}})
    let lat = googleResponse.results[0].geometry.location.lat;
    let lng = googleResponse.results[0].geometry.location.lng;

    const shop = new Shop({
      name,
      description,
      user,
      category,
      address,
      geometry : {
        type: "Point",
        coordinates: [lng,lat]
      }
    });
    
    try {
      const savedShop = await shop.save();
      res.status(201).send({shop: shop._id});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  
  put: async (req, res) =>{
    // validate data before making shop
    //const {error} = saveShopsValidation(req.body);
    //if(error) return res.status(400).send(error.details[0].message);
    
    const shop = await Shop.findOne({_id: req.params.id});
    if(!shop) return res.status(400).send('shop not found');
    
    shop.modified_by = req.user._id;
    shop.name = req.body.name;
    shop.description = req.body.description;
    shop.category = req.body.category;
    
    try {
      const savedShop = await shop.save();
      res.status(201).send(shop);
    } catch (err) {
      res.status(400).send(err);
    }    
  },
  
  get: async (req, res) => {
    try {
     /* const s = await Shop.aggregate().near({
        near: [parseFloat(req.query.lng), parseFloat(req.query.lat)],
        maxDistance: 0.009,
        spherical: true,
        distanceField: "dist"
      });*/
      let s = await Shop.find()
      res.status(200).json(s);
    } catch (err) {
      res.status(400).send("adsad");
    }
  },
  search: async (req, res) => {
    try {
      let result = await Shop.find(
        {$text: {$search: req.body.text}},
        {score: {$meta: "textScore"}})
        .sort({score:{$meta:"textScore"}})
        .limit(shop_search_limit)

      if(!result) return res.status(400).send('Shop List is Empty');
     
      res.status(200).json(result);
    } catch (err) {
      res.status(400).send(err);
    } 
  },
  getById: async (req, res) => {
    try {
      const shop = await Shop.findOne({_id: req.params.id}).lean();
      if(!shop) return res.status(400).send('Shop List is Empty');
      
      // const productLists = ;
      
      shop.products = await Product.find({ shop: shop._id });
      
      res.status(200).json(shop);
    } catch (err) {
      res.status(400).send(err);
    }    
  },
    remove: async (req, res) => {
    
    
    try {
      const d = await Shop.deleteOne({_id: req.params.id});
      res.status(201).send(d);
    } catch (err) {
      res.status(400).send(err);
    }  
  }
}