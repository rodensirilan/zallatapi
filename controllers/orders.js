const Order = require('../model/Order');
const Product = require('../model/Product');
const User = require('../model/User');
const Shop = require('../model/Shop');
const { saveOrderValidation } = require('../validation');
const _ = require('lodash');
const { order_limit } = require('../config');

module.exports = {
  nearOrder: async (req, res) => {
    try {
    const { order } = req.body;
    const orderLists = await Order.findById(order).populate('products.id');
    if(!orderLists) return res.status(400).send('Order List is Empty');

     const shop = await Shop.findById(orderLists.products[0].id.shop);

     let lat = shop.geometry.coordinates[1];
     let lng = shop.geometry.coordinates[0];
     let result = await User.aggregate([{
       $geoNear: {
          near: { type: "Point", coordinates: [ lng,
                lat ] },
          distanceField: "dist.calculated",
          maxDistance: 50 * 1000,
          query: { role: 2, status: 1 },
          spherical: true
       }
     }]).limit(order_limit);
      res.status(200).json(result);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  setBuyer: async (req, res) => {
    const { order, buyer } = req.body;
    const orderLists = await Order.findById(order);
    if(!orderLists) return res.status(400).send('Order List is Empty');
    orderLists.buyer = buyer;
    try {
    const savedOrder = await orderLists.save();
    res.status(201).send(savedOrder);
    } catch (err) {
    res.status(400).send(err);
    }
  },
  get: async (req, res) => {
    try {
      const orderLists = await Order.find().populate('user').populate('products.id');
      if(!orderLists) return res.status(400).send('Order List is Empty');
      
      res.status(200).json(orderLists);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  getById: async (req, res) => {
    try {
      const orderLists = await Order.findById(req.params.id).populate('user').populate('products.id');
      if(!orderLists) return res.status(400).send('Order List is Empty');
      
      res.status(200).json(orderLists);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  post: async (req, res) => {
    // validate data before making order
    const {error} = saveOrderValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    // Create a new order
    const order = new Order({
      user: req.body.user,
      amount: req.body.amount,
      shipping_address: req.body.shipping_address,
      products: req.body.products
    });
    
    try {
      const savedOrder = await order.save();
      res.status(201).send({order: order._id});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  put: async (req, res) => {
    // validate data before making order
    const {error} = saveOrderValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    //check if logged-in user is order creator
    const order = await Order.findOne({ _id: req.params.id, user: req.user._id });
    if(!order) return res.status(400).send('order not found');
    
    // get user using token payload.
    // res.status(201).send(req.user);
  
    // populate data to be edited
    order.user = req.body.user;
    order.amount = req.body.amount;
    order.shipping_address = req.body.shipping_address;
    order.products = req.body.products;
    
    try {
      const savedOrder = await order.save();
      res.status(201).send(savedOrder);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  
  remove: async (req, res) => {
    const order = await Order.findOne({_id: req.params.id});
    order.status = 0;
    try {
      const savedOrder = await order.save();
      res.status(201).send(savedOrder);
    } catch (err) {
      res.status(400).send(err);
    }  
  },
  
  filter: async (req, res) => {
    let orderLists;
    // check if type or value is not empty
    if ( req.body.type || req.body.value ) {
      // nor datas are empty
      // let check which type
      switch(req.body.type) {
        case "user":
          orderLists = await Order.find({user:req.body.value}).populate('user').populate('products');  
        break;

        case "shop":
          // blockers on how to search for shops since orders can contain different products from different shops
          orderLists = await Order.find({shop:req.body.value}).populate('products');
        break;
      }
    } else {
      // one of the data are empty
      orderLists = await Order.find().populate('user').populate('products');
      if(!orderLists) return res.status(400).send('Order List is Empty');
    }
    
    try {
      res.status(200).json(orderLists);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  date_range: async (req, res) => {
    let orderLists;
    if(req.body.from && req.body.to){
      let startDate = req.body.from; 
      let endDate   = req.body.to;
      orderLists = await Order.find({ 
        date: {
          $gte:  new Date(new Date(startDate)),
          $lt:  new Date(new Date(endDate))
        }
      }).populate('user').populate('products'); 
    } else {
      return res.status(400).send('Order List is Empty');
    }

    try {
      res.status(200).json(orderLists);
    } catch (err) {
      res.status(400).send(err);
    }
  }
}