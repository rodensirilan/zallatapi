const Address = require('../model/Address');
const Shop = require('../model/Shop');
const User = require('../model/User');
const _ = require('lodash');
const axios = require('axios')
module.exports = {
  bindToUser: async (req, res) => {
    const { address, user } = req.body
    if(!address || !user) return res.status(400).send('There was an error');
    
    const addressData = await Address.findById(address).lean();
    const userData = await User.findById(user);
    userData.address = {
      street: addressData.address,
      suburb: addressData.province,
      country: addressData.country,
      postal_code: addressData.postal_code
    }
    userData.geometry = addressData.geometry;
    
    try {
      const savedData = await userData.save()
      res.status(200).json(savedData);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  bindToShop: async (req, res) => {
    const { address, shop } = req.body
    if(!address || !shop) return res.status(400).send('There was an error');
    
    const addressData = await Address.findById(address).lean();
    const shopData = await Shop.findById(shop);
    shopData.address = {
      street: addressData.address,
      suburb: addressData.province,
      country: addressData.country,
      postal_code: addressData.postal_code
    }
    shopData.geometry = addressData.geometry;
    
    try {
      const savedData = await shopData.save()
      res.status(200).json(savedData);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  addAddressBookToUser: async (req, res) => {
     const { address, user } = req.body
    if(!address || !user) return res.status(400).send('There was an error');
    
    const userData = await User.findById(user);
    userData.addressBook = address
    
    try {
      const savedData = await userData.save()
      res.status(200).json(savedData);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  addAddressBookToShop: async (req, res) => {
     const { address, shop } = req.body
    if(!address || !shop) return res.status(400).send('There was an error');
    
    const shopData = await Shop.findById(shop);
    shopData.addressBook = address
    
    try {
      const savedData = await shopData.save()
      res.status(200).json(savedData);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  get: async (req, res) => {

    const address = await Address.find({ status:1 });
    if(!address) return res.status(400).send('Address not found');
    
    try {
      res.status(200).json({address});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  post: async (req, res) => {

    const { address, city, province, country, postal_code } = req.body
    
    let response = await axios.get(`${process.env.GMAP_URL}${address},+${country}`)
    let googleResponse = JSON.parse(JSON.stringify(response.data))
    
    if (googleResponse.status == "ZERO_RESULTS") return res.status(400).json({error:{message: "invalid postal_code"}})
    let lat = googleResponse.results[0].geometry.location.lat;
    let lng = googleResponse.results[0].geometry.location.lng;

    const add = new Address({
      address, 
      city, 
      province, 
      country, 
      postal_code,
      geometry : {
        type: "Point",
        coordinates: [lng,lat]
      }
    });
    
    try {
      const savedAddress = await add.save();
      res.status(200).json({savedAddress});
    } catch (err) {
      res.status(400).send(err);
    }
  },
  put: async (req, res) => {
    const { id, address, city, province, country, postal_code } = req.body
    const add = await Address.findOne({ _id: id });
    if(!address) return res.status(400).send('address not found');
    
    let response = await axios.get(`${process.env.GMAP_URL}${address},+${country}`)
    let googleResponse = JSON.parse(JSON.stringify(response.data))
    
    if (googleResponse.status == "ZERO_RESULTS") return res.status(400).json({error:{message: "invalid postal_code"}})
    let lat = googleResponse.results[0].geometry.location.lat;
    let lng = googleResponse.results[0].geometry.location.lng;
       
    add.address = address
    add.modified_by = req.user._id
    add.city = city
    add.province = province
    add.country = country
    add.postal_code = postal_code
    add.geometry = {
      type: "Point",
      coordinates: [lng,lat]
    }
   
    try {
      
      const savedAddress = await add.save();
      res.status(200).json({savedAddress});
    } catch (err) {
      res.status(400).send("asdsad");
    }
  },
  delete: async (req, res) => {
    const address = await Address.findOne({ _id: req.body.id });
    if(!address) return res.status(400).send('Address not found');
    
    address.status = 0;
    
    try {
      const savedAddress = await address.save();
      res.status(200).json(savedAddress);
    } catch (err) {
      res.status(400).send(err);
    }
  }
}