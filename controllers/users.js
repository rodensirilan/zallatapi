const User = require('../model/User');
const bcrypt = require('bcryptjs');
const { registerValidation, loginValidation } = require('../validation');
const jwt = require('jsonwebtoken');
const JWT_SECRET = 'asdasdasdasd';
const axios = require('axios')
const _ = require('lodash');
const { user_limit } = require('../config');

signToken = user => {
  return jwt.sign({
    iss: 'zallatdev',
    sub: user.id,
    iat: new Date().getTime(), // current time
    exp: new Date().setDate(new Date().getDate() + 1) // current time + 1 day ahead
  }, JWT_SECRET);
}

module.exports = {
  relocate: async (req,res) => {
    try {
      // Check if id is valid
      const user = await User.findById(req.user._id);
      if(!user) return res.status(400).send('user not found');

      user.geometry = {
        type: "Point",
        coordinates: [req.body.long,req.body.lat]
      };
      req.user.geometry = {
        type: "Point",
        coordinates: [req.body.long,req.body.lat]
      };
      
      const savedUser = await user.save();
      res.status(201).json(req.user);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  
  near: async (req,res) => {

    let foundUser = await User.findOne({ "_id": req.body.user });
    if (!foundUser)return res.status(400).json({ error: 'user not found'});
    let lat = foundUser.geometry.coordinates[1];
    let lng = foundUser.geometry.coordinates[0];
    let result = await User.aggregate([{
     $geoNear: {
        near: { type: "Point", coordinates: [ lng,
              lat ] },
        distanceField: "dist.calculated",
        maxDistance: req.body.distance * 1000,
        query: { role: 2, status: 1 },
        spherical: true
     }
   }]).limit(user_limit);

   let newr = await Promise.all(_.map(result, async (place) => {
    let distReponse = await axios.get(`${process.env.GMAP_DISTANCE}destinations=${place.geometry.coordinates[1]},${place.geometry.coordinates[0]}&origins=${lat},${lng}`)
    if(distReponse.data.rows[0].elements[0].status !== "NOT_FOUND") {
      return {
        _id: place._id,
        name: place.name,
       // street: place.address.street,
       // city: place.address.city,
      //  country: place.address.country,
      //  postal_code: place.address.postal_code,
       // distReponse: `${process.env.GMAP_DISTANCE}destinations=${place.geometry.coordinates[1]},${place.geometry.coordinates[0]}&origins=${lat},${lng}`,
        distance : distReponse.data.rows[0].elements[0].distance.value,
        duration: distReponse.data.rows[0].elements[0].duration.value
      }
    }
   }));
   
   return res.status(200).json( _.orderBy(newr, ['distance'], ['asc']));
  },
  signin: async (req, res) => {
    const {error} = loginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    const token = signToken(req.user);
    // Respond with token
    res.status(200).json({ token });
  },
  
  secret: async (req, res) => {
    console.log('signed in!');
    // res.json(req.user);
    res.status(200).send('ok');
  },
  
  post: async (req, res) => {

    // validate data before making users
    const {error} = registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    const { email, role, password, address, name } = req.body;

    // Check if there is a user with the same email
    let foundUser = await User.findOne({ "local.email": email });
    if (foundUser) { 
      return res.status(403).json({ error: 'Email is already in use'});
    }

    // Is there a Google account with the same email?
    foundUser = await User.findOne({ 
      $or: [
        { "google.email": email },
        { "facebook.email": email },
      ]
    });
    if (foundUser) {
      // Let's merge them?
      foundUser.methods.push('local')
      foundUser.local = {
        email: email, 
        password: password
      }
     // foundUser.address = address
      foundUser.name = name
      await foundUser.save()
      // Generate the token
      const token = signToken(foundUser);
      // Respond with token
     
      res.status(200).json({ token });
    }

    // check zip for lat lng
    
    let response = await axios.get(`${process.env.GMAP_URL}${address.street},+${address.country}`)
    let googleResponse = JSON.parse(JSON.stringify(response.data))
    
    if (googleResponse.status == "ZERO_RESULTS") return res.status(400).json({error:{message: "invalid address"}})
    let lat = googleResponse.results[0].geometry.location.lat;
    let lng = googleResponse.results[0].geometry.location.lng;
    
    const newUser = new User({ 
      methods: ['local'],
      local: {
        email: email, 
        password: password
      },
      name,
     // address,
      geometry : {
        type: "Point",
        coordinates: [lng,lat]
      },
      role
    });

    await newUser.save();

    // Generate the token
    const token = signToken(newUser);
    // Send a cookie containing JWT

    res.status(200).json({ token });
  },
    
  google: (req, res) => {
    res.status(200).json(req.user.google);
  },
  facebook: (req, res) => {
    res.status(200).json(req.user.facebook);
  },

  put: async (req, res) => {
    const { id, role, password, name, email } = req.body;

    if(!id) return res.status(400).send("Invalid id");
    
    // check if user exists
    let foundUser = await User.findOne({ "local.email": email });
    if (foundUser) { 
      return res.status(403).json({ error: 'Email is already in use'});
    }
    
    try {
      // Check if id is valid
      const user = await User.findById(id);
      if(!user) return res.status(400).send('user not found');
      
      user.local.password = password;
      user.name = name
      user.role = role
      // user.address = address
      user.local.email = email
      
      const savedUser = await user.save();
      res.status(201).json(savedUser);
    } catch (err) {
      res.status(400).send(err);
    }
  },

  get: async (req, res) => {
    const user = await User.find({status:1}).sort('-date');
    if(!user) return res.status(400).send('Email doesnt exist');
    res.status(200).send(user);
  },
  
  delete: async (req, res) => {

    if(!req.body.id) return res.status(400).send("Invalid id");
    
    try {
      // Check if id is valid
      const user = await User.findById(req.body.id);
      if(!user) return res.status(400).send('user not found');
      
      user.status = 0;

      const savedUser = await user.save();
      res.status(201).send('ok');
    } catch (err) {
      res.status(400).send(err);
    }
  }
}