const mongoose = require('mongoose');

// create geo schmea
const GeoSchema = new mongoose.Schema({
    type: {
        type: String,
        default: 'Point', // 'location.type' must be 'Point'
        required: true
    },
    coordinates: {
        type: [Number]
    }
});

const shopSchema = new mongoose.Schema({
  name : {
    type: String,
    required: true,
    min: 2,
    max: 1024
  },
  description: {
    type: String,
    min: 3,
    max: 1024
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  category: {
    type: Number,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  address: {
    street: {
      type: String,
      lowercase: true
    },
    city: {
      type: String,
      lowercase: true
    },
    country: {
      type: String
    },
    postal_code: {
      type: String
    }
  },
  geometry: {
    index: '2dsphere',
    type: GeoSchema
  },
  addressBook: {
    type: Array,
    default: []
  },
  modified_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});
shopSchema.index({ geometry: '2dsphere', name: 'text', description: 'text' });
shopSchema.pre('save', async function (next){
  this.updated_at = new Date().getTime();
  next();
});
module.exports = mongoose.model('Shop', shopSchema);