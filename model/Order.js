const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  buyer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  amount: {
    type: Number,
    required: true,
    min:0,
    default: 0
  },
  shipping_address: {
    type: String,
    required: true
  },
  shop: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  products: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product'
    },
    quantity: {
      type: Number
    }
  }],
  status: {
    type: Number,
    default: 1
  }
});

module.exports = mongoose.model('Order', orderSchema);