const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

// create geo schmea
const GeoSchema = new mongoose.Schema({
    type: {
        type: String,
        default: 'Point', // 'location.type' must be 'Point'
        required: true
    },
    coordinates: {
        type: [Number]
    }
});

const userSchema = new Schema({
  methods: {
    type: [String],
    required: true
  },
  local: {
    email: {
      type: String,
      lowercase: true
    },
    password: {
      type: String
    }
  },
  google: {
    id: {
      type: String
    },
    email: {
      type: String,
      lowercase: true
    }
  },
  facebook: {
    id: {
      type: String
    },
    email: {
      type: String,
      lowercase: true
    }
  },
  date: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  role: {
    type: Number,
    default: 1
  },
  status: {
    type: Number,
    default: 1
  },
  /*address: {
    street: {
      type: String,
      lowercase: true
    },
    city: {
      type: String,
      lowercase: true
    },
    country: {
      type: String
    },
    postal_code: {
      type: String
    }
  },*/
  geometry: {
    index: '2dsphere',
    type: GeoSchema
  },
  name: {
    type: String
  },
  addressBook: {
    type: Array,
    default: []
  }
});
userSchema.index({ geometry: '2dsphere' });
userSchema.pre('save', async function (next){
  try {
    console.log('entered');
    if (!this.methods.includes('local')) {
      next();
    }
    
    const user = this;
    
    if (!user.isModified('local.password')) {
      next();
    }
    // Generate a salt
    const salt = await bcrypt.genSalt(10);
    // Generate a password hash (salt + hash)
    const passwordHash = await bcrypt.hash(this.local.password, salt);
    // Re-assign hashed version over original, plain text password
    this.local.password = passwordHash;
    this.updated_at = new Date().getTime();
    console.log('exited');
    next();
  } catch (error) {
    next(error);
  }
});

userSchema.methods.isValidPassword = async function (newPassword) {
  try {
    return await bcrypt.compare(newPassword, this.local.password);
  } catch (error) {
    throw new Error(error);
  }
}

module.exports = mongoose.model('User', userSchema);