const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
  name : {
    type: String,
    required: true,
    min: 3,
    max: 255
  },
  description: {
    type: String,
    min: 3,
    max: 1024
  },
  role_number: {
    type: Number,
    required: true
  },
  data: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  modified_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});
roleSchema.pre('save', async function (next){
  this.updated_at = new Date().getTime();
  next();
});
module.exports = mongoose.model('Role', roleSchema);