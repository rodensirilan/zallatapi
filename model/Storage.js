const mongoose = require('mongoose');

const storageSchema = new mongoose.Schema({
  type : {
    type: String,
    required: true,
    min: 3,
    max: 255
  },
  name: {
    type: String,
    required: true,
    min: 3,
    max: 255
  },
  parent: {
    type: String,
    required: true,
    min: 3,
    max: 255
  },
  path: {
    type: String,
    required: true
  },
  status: {
    type: Number,
    default: 1
  },
  date: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  modified_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});
storageSchema.pre('save', async function (next){
  this.updated_at = new Date().getTime();
  next();
});
module.exports = mongoose.model('Storage', storageSchema);