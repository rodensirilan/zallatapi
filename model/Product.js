const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name : {
    type: String,
    required: true,
    min: 2,
    max: 1024
  },
  user : {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  description: {
    type: String,
    min: 3,
    max: 1024
  },
  price: {
    type: Number,
    min: 0,
    default: 0
  },
  category: {
    type: Number,
    required: true,
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop'
  },
  date: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  modified_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});
productSchema.pre('save', async function (next){
  this.updated_at = new Date().getTime();
  next();
});
module.exports = mongoose.model('Product', productSchema);