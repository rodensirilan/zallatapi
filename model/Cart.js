const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
  date: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  user: {
     type: mongoose.Schema.Types.ObjectId,
     ref: 'User'
  },
  products: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product'
    },
    quantity: {
      type: Number
    }
  }],
  status: {
    type: Number,
    default: 1
  }
});
cartSchema.pre('save', async function (next){
  this.updated_at = new Date().getTime();
  next();
});
module.exports = mongoose.model('Cart', cartSchema);