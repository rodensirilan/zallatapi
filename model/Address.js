const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

// create geo schmea
const GeoSchema = new mongoose.Schema({
    type: {
        type: String,
        default: 'Point', // 'location.type' must be 'Point'
        required: true
    },
    coordinates: {
        type: [Number]
    }
});

const AddressSchema = new Schema({
  address: {
    type: String,
    required: true
  },
  city: {
    type: String
  },
  province: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  country: {
    type: String
  },
  status: {
    type: Number,
    default: 1
  },
  postal_code: {
    type: String
  },
  geometry: {
    index: '2dsphere',
    type: GeoSchema
  },
  modified_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});
AddressSchema.index({ geometry: '2dsphere' });
AddressSchema.pre('save', async function (next){
  this.updated_at = new Date().getTime();
  next();
});
module.exports = mongoose.model('Address', AddressSchema);