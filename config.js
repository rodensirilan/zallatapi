const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  stripe_secret: process.env.STRIPE_SECRET_KEY,
  limiter: parseInt(process.env.LIMITER),
  shop_limit: parseInt(process.env.SHOP_LIMITER || process.env.LIMITER),
  order_limit: parseInt(process.env.ORDER_LIMITER || process.env.LIMITER),
  user_limit: parseInt(process.env.USER_LIMITER || process.env.LIMITER),
  uploadFolder: process.env.UPLOADS || './uploads/',
  product_search_limit: parseInt(process.env.PRODUCT_SEARCH_LIMIT || process.env.LIMITER),
  shop_search_limit: parseInt(process.env.SHOP_SEARCH_LIMIT || process.env.LIMITER)
};